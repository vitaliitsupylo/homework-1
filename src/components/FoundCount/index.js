import React from 'react';
import './found-count.scss';

export default function FoundCount({amount}) {
    return (
        <div className="found-count">
            {(amount) ? <p>{amount}</p> : <img src="./images/search.svg" alt=""/>}
        </div>
    )
}