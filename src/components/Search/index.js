import React from "react";
import './search.scss';

export default function Index({value, handler}) {

    return (
        <div className="search">
            <input className="search__input" placeholder="Введите имя гостя для поиска" type="search" value={value} onChange={handler}/>
        </div>
    )
}