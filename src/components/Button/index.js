import React from 'react';
import './button.scss';

export default function Button({children, handler}) {
    return (
        <button onClick={handler} className="button" type="button"><span>{children}</span></button>
    )
}