import React from 'react';
import './listItem.scss';

const ListItem = ({user, handler}) => {
    return (
        <div className={user.arrived ? "list-item success" : "list-item"}>
            <div className="list-item__content">
                <p>Гость <span>{user.name}</span> работает в компании <span>{user.company}</span></p>
                <p>Его контакты:</p>
                <p><b>{user.phone}</b></p>
                <p><b>{user.address}</b></p>
            </div>
            <button className="list-item__btn" onClick={handler(user)}>Прибыл!</button>
        </div>
    );
};

export default ListItem;


