import React, {Component} from 'react';
import ListItem from './components/ListItem';
import Search from './components/Search';
import data from './guests.json';
import FoundCount from './components/FoundCount';
import Button from './components/Button';
import './App.scss';


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: '',
            countVisible: 10,
            stepVisible: 10,
            filteredData: null,
            data: data.map(user => {
                user.arrived = false;
                return user;
            })
        }
    }

    makeUserArrived = user => () => {
        const {data} = this.state;
        const changedData = data.map(item => {
            if (item.index === user.index) {
                item.arrived = !item.arrived;
            }
            return item;
        });

        this.setState({
            data: changedData
        })
    };

    searchUser = (even) => {
        const {data} = this.state;
        const newElem = data.filter((elem) => {
            const {...newElem} = elem;
            delete newElem['_id'];
            delete newElem['arrived'];
            const valuesObj = Object.values(newElem).join(' ').toLowerCase();

            return valuesObj.indexOf(`${even.target.value.toLowerCase()}`) > -1;
        });

        this.setState({
            value: even.target.value,
            filteredData: (even.target.value.length > 0) ? newElem : [...data]
        });
    };

    getCountFromFiltered = () => {
        return (this.state.value.length === 0) ? null : `${this.state.filteredData.length}`;
    };

    moveLoadMoreGuests = (loadAll) => () => {
        const {data, countVisible, stepVisible} = this.state;
        const balance = data.length - (countVisible + stepVisible);

        const countStepElem = (stepVisible + countVisible <= data.length - 1) ?
            countVisible + stepVisible : data.length;

        this.setState({
            countVisible: loadAll ? countStepElem : data.length,
            stepVisible: (balance < stepVisible) ? balance : stepVisible
        });
    };

    render = () => {
        const {data, value, filteredData, countVisible, stepVisible} = this.state;
        const {makeUserArrived, searchUser, moveLoadMoreGuests} = this;
        const list = (filteredData !== null) ? filteredData : data;

        return (
            <div className="guests">
                <div className="guests__wrap">
                    <div className="guests__head">
                        <div className="guests__top">
                            <p className="guests__title">Список гостей</p>
                            <FoundCount amount={this.getCountFromFiltered()}/>
                        </div>
                        <Search value={value} handler={searchUser}/>
                    </div>
                    <div className="guests__body">
                        {list.map((item, key) => {
                            return (
                                (key < countVisible) ? <ListItem
                                    key={key}
                                    user={item}
                                    handler={makeUserArrived}
                                /> : null)
                        })}
                    </div>
                    {(list.length > countVisible && countVisible < list.length) ?
                        <div className="guests__footer">
                            <Button handler={moveLoadMoreGuests(true)}>Загрузить еще {stepVisible}</Button>
                            <Button handler={moveLoadMoreGuests()}>Загрузить все</Button>
                        </div> : null}
                </div>
            </div>
        )
    }
}

export default App;
